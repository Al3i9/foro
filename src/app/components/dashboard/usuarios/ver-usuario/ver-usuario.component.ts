import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {

  form! : FormGroup;

  constructor(private activateRoute: ActivatedRoute,
            private usuarioService: UsuarioService,
            private router: Router,
            private fb: FormBuilder) {
    this.activateRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      const usuario = this.usuarioService.buscarUsuario(id);
      console.log(usuario);

      console.log(Object.keys(usuario).length);
      

      //Verifico que la longitud del objeto es cero
      if(Object.keys(usuario).length === 0){
        this.router.navigate(['/dashboard/usuarios']);
      }

      this.form = this.fb.group({
            nombres: ['', Validators.required],
            apellidos: ['', Validators.required],
            email: ['', Validators.required],
            celular: ['', Validators.required],
            fecha: ['', Validators.required],
            hora: ['', Validators.required],
            descripcion: ['', Validators.required]
      });
      this.form.patchValue({
            nombres: usuario.nombres,
            apellidos: usuario.apellidos,
            email: usuario.email,
            celular: usuario.celular,
            fecha: usuario.fecha,
            hora: usuario.hora,
            descripcion: usuario.descripcion
      });
    });
   }

  ngOnInit(): void {
  }

  Volver(): void{
    this.router.navigate(['/dashboard/usuarios']);
  }
}
