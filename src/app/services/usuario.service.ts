import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [
    // { nombres: 'Julia', apellidos: 'Caceres', email: 'Jul@gmail.com', celular: 60701234, fecha: '2022-06-13', hora: '09:00', descripcion:'Quisiera saber el trabajo que realiza'},
    // { nombres: 'Andres', apellidos: 'Lopez', email: 'Alo@gmail.com', celular: 60123480, fecha: '2022-06-16', hora: '14:00', descripcion:'Contactenos'},
    // { nombres: 'Amelia', apellidos: 'Zuarez', email: 'Zua123@gmail.com', celular: 70905544, fecha: '2022-06-20', hora: '10:00', descripcion:'Quisiera una reunion'}
  ];

  constructor() { }

  getUsuario(): UsuarioDataI[]{
    //Slice retorna una copia del array
    return this.listUsuarios.slice();
    
  }

  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
    // localStorage.setItem('listUsuario',JSON.stringify(this.listUsuarios));
    // this.guardarInformacion();

    if(localStorage.getItem('guardado') === null){
      this.listUsuarios = [];
      this.listUsuarios.unshift(usuario);
      localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    }else{
      this.listUsuarios = JSON.parse(localStorage.getItem('guardado') || '');
      this.listUsuarios.unshift(usuario);
      localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    }
    // console.log(this.listUsuarios, 'lista de guardarMensaje');
    // this.listUsuarios.reset()
  }

  eliminarUsuario(usuario: any){
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.nombres.toString() !== usuario.toString();
    })
    localStorage.setItem('guardado', JSON.stringify(this.listUsuarios));
    // this.guardarInformacion();
  }

  buscarUsuario(id: string): UsuarioDataI{
    //o retorna un json {} vacio
    return this.listUsuarios.find(element => element.nombres === id) || {} as UsuarioDataI;
  }
    
  modificarUsuario(user: any){
    this.eliminarUsuario(user.nombres);
    this.agregarUsuario(user);
  }

  obtenerLocalStorage(){
    let listUsuarios = JSON.parse(localStorage.getItem('guardado') || '');
    console.log(listUsuarios);
    
    // this.listUsuarios = usuarioLista
    // console.log(usuarioLista, 'obtener LocalStorage');
    
  }

  // guardarMensaje(){
    // if(!localStorage.getItem('listUsuario')){
    //   console.log(this.listUsuarios);
    //   localStorage.setItem('listUsuario', JSON.stringify(this.listUsuarios));
    //   let guardar = localStorage.getItem('listUsuario');
    //   this.listUsuarios = JSON.parse(guardar || '{}');
    //   console.log(this.listUsuarios[0]);
    // }else{
    //   let guardar = localStorage.getItem('listUsuario');
    //   this.listUsuarios = JSON.parse(guardar || '{}');
    // }
    // if(localStorage.getItem('guardado') === null){
    //   this.listUsuarios = [];
    //   this.listUsuarios.unshift(guardar);
    //   localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    // }else{
    //   this.listUsuarios = JSON.parse(localStorage.getItem('guardado') || '');
    //   this.listUsuarios.unshift(guardar);
    //   localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    // }
    // console.log(this.listUsuarios, 'lista de guardarMensaje');
  //  this.formulario.reset() 

  // }
}
